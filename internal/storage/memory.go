package storage

import (
	"fmt"
	"gitlab.com/aamoiseev/otus-calendar/internal/contracts"
	"sort"
	"sync"
	"time"
)

type memory struct {
	sync.RWMutex
	data map[string]record
}

type record struct {
	t time.Time
	v interface{}
}

func (m *memory) Insert(key string, value interface{}) error {
	m.Lock()
	defer m.Unlock()

	_, found := m.data[key]
	if found {
		return fmt.Errorf(`record with key "%s" already exists`, key)
	}

	m.data[key] = record{time.Now(), value}

	return nil
}

func (m *memory) Update(key string, value interface{}) error {
	m.Lock()
	defer m.Unlock()

	record, found := m.data[key]
	if !found {
		return fmt.Errorf(`record with key "%s" does not exist`, key)
	}

	record.v = value

	m.data[key] = record

	return nil
}

func (m *memory) Delete(key string) (bool, error) {
	m.Lock()
	defer m.Unlock()

	_, found := m.data[key]
	if !found {
		return false, nil
	}

	delete(m.data, key)

	return true, nil
}

func (m *memory) Get(key string) (interface{}, error) {
	m.RLock()
	defer m.RUnlock()

	record, found := m.data[key]
	if !found {
		return nil, fmt.Errorf(`record with key "%s" does not exist`, key)
	}

	return record.v, nil
}

func (m *memory) List() ([]interface{}, error) {
	m.RLock()
	defer m.RUnlock()

	records := make([]record, 0, len(m.data))
	for _, r := range m.data {
		records = append(records, r)
	}

	// Sort By Add Time
	sort.Slice(records, func (i, j int ) bool {
		return records[i].t.Before(records[j].t)
	})

	list := make([]interface{}, len(records))
	for i, r := range records {
		list[i] = r.v
	}

	return list, nil
}

func NewMemory() contracts.Storage {
	return &memory{
		data: make(map[string]record),
	}
}
