package calendar

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/aamoiseev/otus-calendar/internal/storage"
	"testing"
	"time"
)

func TestCalendar_AddEvent(t *testing.T) {
	m := storage.NewMemory()
	c := NewCalendar(m)

	evt := Event{
		Time: time.Now().Add(time.Hour),
	}

	err := c.AddEvent(&evt)
	assert.Nil(t, err)

	actual, err := c.GetEvent(evt.Key)
	assert.Nil(t, err)
	assert.Equal(t, evt, *actual)
}

func TestCalendar_UpdateEvent(t *testing.T) {
	m := storage.NewMemory()
	c := NewCalendar(m)

	evt := Event{
		Time: time.Now().Add(time.Hour),
	}

	err := c.AddEvent(&evt)
	assert.Nil(t, err)

	evt.Time = time.Now().Add(2 * time.Hour)
	err = c.UpdateEvent(evt)
	assert.Nil(t, err)

	actual, err := c.GetEvent(evt.Key)
	assert.Nil(t, err)
	assert.Equal(t, evt, *actual)
	assert.Equal(t, evt, *actual)
}

func TestCalendar_RemoveEvent(t *testing.T) {
	m := storage.NewMemory()
	c := NewCalendar(m)

	evt := Event{
		Time: time.Now().Add(time.Hour),
	}

	err := c.AddEvent(&evt)
	assert.Nil(t, err)

	ok, err := c.RemoveEvent(evt.Key)
	assert.True(t, ok)

	actual, err := c.GetEvent(evt.Key)
	assert.Nil(t, actual)
	assert.Errorf(t, err, `record with key "%s" does not exist`, evt.Key)
}

func TestCalendar_ListEvents(t *testing.T) {
	m := storage.NewMemory()
	c := NewCalendar(m)

	events := []Event{
		{
			Time: time.Now().Add(time.Hour),
		},
		{
			Time: time.Now().Add(2 * time.Hour),
		},
		{
			Time: time.Now().Add(3 * time.Hour),
		},
	}

	for i := range events {
		err := c.AddEvent(&events[i])
		assert.Nil(t, err)
	}

	list, err := c.ListEvents()
	assert.Nil(t, err)
	assert.Equal(t, events, list)
}
