package calendar

import "time"

// Event represents a Calendar Event
type Event struct {
	Key string
	Time time.Time
}
